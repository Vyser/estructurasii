/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <bitset>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
				   struct cache_field_size *field_size)
{
	/* Check if parameters are valid */
	if (cache_params.size <= 0 || cache_params.asociativity <= 0 || cache_params.block_size <= 0)
	{
		return PARAM;
	}
	if ((cache_params.size % 2 == 1) || (cache_params.asociativity % 2 == 1) || (cache_params.block_size % 2 == 1))
	{
		return PARAM;
	}

	/* Get offset, idx and tag size */
	field_size->offset = log2(cache_params.block_size);
	field_size->idx = log2(cache_params.size * KB / (cache_params.block_size * cache_params.asociativity));
	field_size->tag = ADDRSIZE - field_size->idx - field_size->offset;
	return OK;
}

void address_tag_idx_get(long address,
						 struct cache_field_size field_size,
						 int *idx,
						 int *tag)
{
	/* Get tag */
	bitset<ADDRSIZE> tag_s(address);
	tag_s >>= field_size.idx + field_size.offset;
	*tag = (int)(tag_s.to_ulong());
	/* Get idx */
	bitset<ADDRSIZE> index_s(address);
	index_s <<= field_size.tag;
	index_s >>= field_size.tag + field_size.offset;
	*idx = (int)(index_s.to_ulong());
}

int srrip_replacement_policy(int idx,
							 int tag,
							 int associativity,
							 bool loadstore,
							 entry *cache_blocks,
							 operation_result *result,
							 bool debug)
{
	/* Check if parameters are valid */
	if (idx < 0 || tag < 0 || associativity < 1)
	{
		return PARAM;
	}
	int M = (associativity > 2) ? 2 : 1;

	bool hit_miss = false;

	/* if HIT */
	for (int i = 0; i < associativity; i++)
	{
		if (cache_blocks[i].tag == tag && cache_blocks[i].valid)
		{
			hit_miss = true;
			cache_blocks[i].rp_value = 0;

			/* No eviction for HITS */
			result->dirty_eviction = false;

			/* if operation is LOAD */
			if (!loadstore)
			{
				result->miss_hit = HIT_LOAD;
			}
			/* if operation is STORE */
			else
			{
				result->miss_hit = HIT_STORE;
				cache_blocks[i].dirty = true;
			}
			return OK;
		}
	}

	/* if MISS */

	if (!hit_miss)
	{
		bool distant_found = false;
		int update = associativity + 1;
		int cache_quant = 0;

		/* Is cache full? */
		for (int i = 0; i < associativity; i++)
		{
			if (cache_blocks[i].valid)
			{
				cache_quant++;
			}
		}

		for (int i = 0; i < associativity; i++)
		{

			/* Check for the first block apt for replacement */
			if (cache_blocks[i].rp_value == pow(2, M) - 1)
			{
				distant_found = true;

				/* Case cache full */
				if (cache_quant == associativity)
				{
					/* Dirty eviction depending on dirty bit */
					result->dirty_eviction = cache_blocks[i].dirty;
					result->evicted_address = cache_blocks[i].tag;
				}

				/* Case cache not full */
				else
				{
					result->evicted_address = 0;
					result->dirty_eviction = false;
				}

				/* Update missed block entry */
				cache_blocks[i].tag = tag;
				cache_blocks[i].valid = true;
				cache_blocks[i].rp_value = pow(2, M) - 2;

				/* if operation is LOAD */
				if (!loadstore)
				{
					cache_blocks[i].dirty = false;
					result->miss_hit = MISS_LOAD;
				}

				/* if operation is STORE */
				else
				{
					cache_blocks[i].dirty = true;
					result->miss_hit = MISS_STORE;
				}
				return OK;
			}
		}

		/* No available block found, increase rp_value until first available */

		while (distant_found == false)
		{

			for (int i = 0; i < associativity; i++)
			{
				cache_blocks[i].rp_value++;
			}
			for (int i = 0; i < associativity; i++)
			{
				if (cache_blocks[i].rp_value == pow(2, M) - 1)
				{
					distant_found = true;
					update = i;
					break;
				}
			}
		}

		/* if set had to be updated */

		if (update != associativity + 1)
		{
			/* Dirty eviction depending on dirty bit */
			result->dirty_eviction = cache_blocks[update].dirty;
			result->evicted_address = cache_blocks[update].tag;

			/* Update missed block entry */
			cache_blocks[update].tag = tag;
			cache_blocks[update].valid = true;
			cache_blocks[update].rp_value = pow(2, M) - 2;

			/* if operation is LOAD */
			if (!loadstore)
			{
				cache_blocks[update].dirty = false;
				result->miss_hit = MISS_LOAD;
			}
			/* if operation is STORE */
			else
			{
				cache_blocks[update].dirty = true;
				result->miss_hit = MISS_STORE;
			}
			return OK;
		}
	}
	return ERROR;
}

int lru_replacement_policy(int idx,
						   int tag,
						   int associativity,
						   bool loadstore,
						   entry *cache_blocks,
						   operation_result *result,
						   bool debug)
{
	/* Check for valid parameters */
	if (idx < 0 || tag < 0 || associativity < 1)
	{
		return PARAM;
	}
	bool hit_miss = false;

	/* Check if there's a hit */
	for (int i = 0; i < associativity; i++)
	{
		if (cache_blocks[i].tag == tag && cache_blocks[i].valid)
		{ // The tag matches the searched address and it's valid
			hit_miss = true;

			/* No eviction for HITS */
			// https://open-cas.github.io/cache_operations.html
			result->dirty_eviction = false;

			/* if operation is LOAD */
			if (!loadstore)
			{
				result->miss_hit = HIT_LOAD;
			}

			/* if operation is STORE */
			else
			{
				result->miss_hit = HIT_STORE;
				/* Dirty bit is set to true because the operation is a STORE */
				cache_blocks[i].dirty = true;
			}

			/* Reorder array according to replacement policy value */
			for (int j = 0; j < associativity; j++)
			{
				if (cache_blocks[j].rp_value > cache_blocks[i].rp_value)
				{
					cache_blocks[j].rp_value--;
				}
			}

			/* Hit blocks are MRU */
			cache_blocks[i].rp_value = associativity - 1;
			break;
		}
	}

	/* if there was a miss in the operation */
	if (!hit_miss)
	{

		/* Search for the LRU */
		for (int i = 0; i < associativity; i++)
		{
			if (cache_blocks[i].rp_value == LRU)
			{

				/* Dirty eviction depending on dirty bit */
				result->dirty_eviction = cache_blocks[i].dirty;
				result->evicted_address = cache_blocks[i].tag; // This is the address that got evicted

				/* if operation is LOAD */
				if (!loadstore)
				{
					cache_blocks[i].dirty = false;
					result->miss_hit = MISS_LOAD;
				}

				/* if operation is STORE */
				else
				{
					cache_blocks[i].dirty = true; // dirty bit is true because the operation changes the block
					result->miss_hit = MISS_STORE;
				}

				/* Reorder array according to replacement policy value */
				for (int j = 0; j < associativity; j++)
				{
					if (cache_blocks[j].rp_value > cache_blocks[i].rp_value)
					{
						cache_blocks[j].rp_value--;
					}
				}

				/* newly introduced block is MRU */
				cache_blocks[i].rp_value = associativity - 1;

				/* Update missed block entry */
				cache_blocks[i].tag = tag;
				cache_blocks[i].valid = true;
				break;
			}
		}
	}
	return OK;
}

int nru_replacement_policy(int idx,
						   int tag,
						   int associativity,
						   bool loadstore,
						   entry *cache_blocks,
						   operation_result *operation_result,
						   bool debug)
{

	if (idx < 0 || tag < 0 || associativity < 1)
	{
		return PARAM;
	}

	bool hit_miss = false;
	int way = 0;

	for (int i = 0; i < associativity; i++)
	{
		/* Check for cache hit */
		if (cache_blocks[i].tag == tag && cache_blocks[i].valid)
		{
			hit_miss = true;
			way = i;
			break;
		}
	}

	/* HIT! there was a hit looking for the block */
	if (hit_miss)
	{

		/* NRU bit is set to 0 */
		cache_blocks[way].rp_value = 0;

		/* No eviction for HITS */
		operation_result->dirty_eviction = false;

		/* if operation is LOAD */
		if (!loadstore)
		{
			operation_result->miss_hit = HIT_LOAD;
		}

		/* if operation is STORE */
		else
		{
			/* dirty bit is true because the operation changes the block */
			cache_blocks[way].dirty = true;
			operation_result->miss_hit = HIT_STORE;
		}
	}

	/* In case of miss*/
	if (!hit_miss)
	{
		bool found_1 = false; // Variable to scan for NRU blocks to replace
		int where = 0; // Location of the NRU block to replace

		/* Search for the first rp value = 1 */
		for (int j = 0; j < associativity; j++)
		{
			if (cache_blocks[j].rp_value == 1 && (!found_1) )
			{
				found_1 = true;
				where = j;
				break;
			}
		}

		/* There was an NRU block fit for replacement */
		if (found_1)
		{

			/* Dirty eviction set to 1 if dirty bit = 1 */
			operation_result->dirty_eviction = cache_blocks[where].dirty;

			/* Evicted address = tag of evicted block */
			operation_result->evicted_address = cache_blocks[where].tag;

			/* Update block in cache */
			cache_blocks[where].tag = tag;
			cache_blocks[where].rp_value = 0;
			cache_blocks[where].valid = true;

			/* if operation is LOAD */
			if (!loadstore)
			{
				operation_result->miss_hit = MISS_LOAD;
				cache_blocks[where].dirty = false;
			}

			/* if operation is STORE */
			else
			{
				operation_result->miss_hit = MISS_STORE;
				cache_blocks[where].dirty = true; // dirty bit is true because the operation changes the block
			}
		}

		/* A block to replace WAS NOT found */
		else
		{
			/* Set all rp values of the line to 1 */
			for (int k = 0; k < associativity; k++)
			{
				cache_blocks[k].rp_value = 1;
			}

			/* Same logic as above once all values are changed */
			bool found_1 = false;
			int where = 0;
			for (int j = 0; j < associativity; j++)
			{
				if (cache_blocks[j].rp_value == 1 && found_1 == false)
				{
					found_1 = true;
					where = j;
					break;
				}
			}
			if (found_1)
			{
				operation_result->dirty_eviction = cache_blocks[where].dirty;
				operation_result->evicted_address = cache_blocks[where].tag;
				cache_blocks[where].tag = tag;
				cache_blocks[where].rp_value = 0;
				cache_blocks[where].valid = true;
				if (!loadstore)
				{
					operation_result->miss_hit = MISS_LOAD;
					cache_blocks[where].dirty = false;
				}
				else
				{
					operation_result->miss_hit = MISS_STORE;
					cache_blocks[where].dirty = true;
				}
			}
		}
	}

	return OK;
}

/* Create cache matrix from given number of ways, idx_size, and replacement policy */
entry **cache_blocks(int ways, int idx_size, int rp)
{

	/* Size of the set */
	int set = pow(2, idx_size);
	/* Array of pointers to access each set */
	entry **cache_matrix = new entry *[set];
	for (int i = 0; i < set; i++)
	{
		/* Initialize entries in the matrix for each set */
		cache_matrix[i] = new entry[ways];
	}
	/* i = Rows, j = Columns */
	/* Fill out the cache matrix with initial entry values according to the replacement policy */
	if (rp == LRU)
	{ // LRU
		for (int i = 0; i < set; i++)
		{
			for (int j = 0; j < ways; j++)
			{
				/* Fill with cleared/false values */
				cache_matrix[i][j].valid = false;
				cache_matrix[i][j].dirty = false;
				cache_matrix[i][j].tag = 0;
				/* For LRU policy, replacement value is initially set as entry position on set */
				cache_matrix[i][j].rp_value = j;
				cache_matrix[i][j].obl_tag = false;
			}
		}
	}
	else if (rp == NRU)
	{ // NRU
		for (int i = 0; i < set; i++)
		{
			for (int j = 0; j < ways; j++)
			{
				/* Initalize with cleared/false values */
				cache_matrix[i][j].valid = false;
				cache_matrix[i][j].dirty = false;
				cache_matrix[i][j].tag = 0;
				/* For NRU replacement value is initially set as 1 */
				cache_matrix[i][j].rp_value = 1;
				cache_matrix[i][j].obl_tag = false;
			}
		}
	}
	else if (rp == RRIP)
	{ // S-RRIP
		int M = 0;
		for (int i = 0; i < set; i++)
		{
			for (int j = 0; j < ways; j++)
			{
				cache_matrix[i][j].valid = false;
				cache_matrix[i][j].dirty = false;
				cache_matrix[i][j].tag = 0;
				cache_matrix[i][j].obl_tag = false;
				/* M value is set according to associativity */
				if (ways <= 2)
				{
					M = 1;
				}
				else
				{
					M = 2;
				}
				/* For S-RRIP policy, initial replacement values are computed from M value */
				cache_matrix[i][j].rp_value = pow(2, M) - 1;
			}
		}
	}
	return cache_matrix;
}