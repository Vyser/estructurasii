#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <bits/stdc++.h>
#include <time.h>
#include <L1cache.h>
#include <debug_utilities.h>

#define KB 1024
#define ADDRSIZE 32

using namespace std;

/* Helper functions */

/* Print results function*/
void print_usage(int size, int associativity, int block_size, int rp, float load_h, float load_m, float store_h, float store_m, int dirty_evic)
{
  float total_m = load_m + store_m;
  float total_h = load_h + store_h;
  float missrate = (float)total_m/(float)(total_h + total_m);

  cout << "_______________________________________" << endl;
  cout << "Cache parameters:" << endl;
  cout << "_______________________________________\n" << endl;
  cout << "Cache Size (KB):               "<< size << endl;
  cout << "Cache Associativity:           "<< associativity << endl;
  cout << "Cache Block Size (bytes):      "<< block_size << endl;
  cout << "_______________________________________" << endl;
  cout << "Simulation results:" << endl;
  cout << "_______________________________________\n" << endl;
  cout << "Overall miss rate::            "<< total_m/(total_h + total_m) << endl;
  cout << "Read miss rate:                "<< load_m/(total_h + total_m) << endl;
  cout << fixed;
  cout << setprecision(0);
  cout << "Dirty evictions:               "<< dirty_evic << endl;
  cout << "Load misses:                   "<< load_m << endl;
  cout << "Store misses:                  "<< store_m << endl;
  cout << "Total misses:                  "<< total_m << endl;
  cout << "Load hits:                     "<< load_h << endl;
  cout << "Store hits:                    "<< store_h << endl;
  cout << "Total hits:                    "<< total_h << endl;
  cout << "_______________________________________" << endl;

  exit(0);
}


  /* main */
int main(int argc, char * argv []) {
  
  /* Parse arguments */
  int size, asociativity, block_size, rp;
  /* Arguments stored in variables */
  for(size_t i = 1; i < argc; i++){ // reads stdin
    if(strcmp(argv[i], "-t") == 0){ // when argv[i] = keyword
      size = atoi(argv[i+1]); // store the parameter (located at argv[i+1]) for each keyword
    }
    else if(strcmp(argv[i], "-l") == 0){
      block_size = atoi(argv[i+1]);
    }
    else if(strcmp(argv[i], "-a") == 0){
      asociativity = atoi(argv[i+1]);
    }
    else if(strcmp(argv[i], "-rp") == 0){
      rp = atoi(argv[i+1]);
    }
  }

  /* Instance of cache_params struct initialized and updated with corresponding params*/
  struct cache_params cache_p;
  cache_p.size = size;
  cache_p.block_size = block_size;
  cache_p.asociativity = asociativity;
  /* Same thing for cache_field_size */
  struct cache_field_size field_s;
  field_size_get(cache_p, &field_s);

  /* cache is created once params have been loaded and field size calculated */
  entry **get_cache = cache_blocks(asociativity, field_s.idx, rp);

  /* operation result instance is created */
  struct operation_result res;

  /* Get trace's lines and start your simulation */
  int loadstore, idx, tag, ic;
  int debug = 0;
  char lineP[200];
  char readAddress[10]; // For storing the adress read from trace file (string)
  long address;        // For storing the address, once converted to long (from string)
  float load_h = 0;
  float store_h = 0;
  float load_m = 0;
  float store_m = 0;
  int dirty_evic = 0;
  int it = 0; // To count the total of executed instructions (for cpu time calculations)

  while(fgets(lineP, 50, stdin)!= NULL){
    if(lineP[0] == 35){ // if line is valid
      sscanf(lineP, "%*s %d %s %d", &loadstore, readAddress, &ic);
      it = it + ic; // instruction total is updated
      address = strtol(readAddress, NULL, 16); // hex-string address is converted to long
    }
    else{
      break;
    }
    address_tag_idx_get(address, field_s, &idx, &tag); //tag and index are obtained from current address value
    switch (rp)  /* The replacement policy corresponding to the given rp value is called */
    {
    case LRU:
      lru_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break;
    case NRU:
      nru_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break;
    case RRIP:
      srrip_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break; 
    default:
      lru_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break;
    }

    /* Stats are updated for each line read from trace file from obtained results */
    if (res.miss_hit == HIT_LOAD){
      load_h++;
    } 
    else if (res.miss_hit == MISS_LOAD){
      load_m++;
    }
    else if (res.miss_hit == HIT_STORE){
      store_h++;
    }
    else if (res.miss_hit == MISS_STORE){
      store_m++;
    }
    if (res.dirty_eviction == true){
      dirty_evic++;
    }
  }

  /* Call to print function */
  print_usage(size, asociativity, block_size, rp, load_h, load_m, store_h, store_m, dirty_evic);
  
return 0;
}