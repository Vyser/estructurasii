/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	bool loadstore,
       	                    	entry* l1_cache_blocks,
       	                  	 	entry* vc_cache_blocks,
        	                 	operation_result* l1_result,
              	            	operation_result* vc_result,
                	         	bool debug)
{

	/* For checking hits, initialized as false */ 
	bool l1_hit = false;
	bool vc_hit = false;

	/* to keep VC from "working" if L1 is not full, initialized as false */
	bool l1_full = false;

	/*To count valid blocks in L1 */
	int valid_blocks_cnt = 0;

	/* Check if L1 is full */
	for (size_t i = 0; i < l1_vc_info->l1_assoc; i++)
	{
		if (l1_cache_blocks[i].valid == true)
		{
			valid_blocks_cnt++;	
		}
	}
	/* If valid blocks in L1 == L1 size -> L1 is full -> Activate VC */
	if (valid_blocks_cnt == l1_vc_info->l1_assoc)
	{
		/*VC must be used*/
		l1_full = true;
	}	
		

	/*Check for hit in L1*/	
	for (int i = 0; i < l1_vc_info->l1_assoc; i++)
	{
		if (l1_vc_info->l1_tag == l1_cache_blocks[i].tag && l1_hit == false)
		{
			/* If tag value is found, there is a hit in L1 */
			l1_hit = true;
		}
	}

	/*Check for hit in VC*/
	for (int i = 0; i < l1_vc_info->vc_assoc; i++)
	{
		if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag && vc_hit == false)
		{
			/* If tag value is found, there is a hit in VC */
			vc_hit = true;
		}
	}

	/* In case of hit in L1, simply execute LRU on L1 as usual*/
	if(l1_hit){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);
		
	}

	/* In case of miss in L1 but hit in VC, LRU is ran on L1 to get evicted adress result*/
	if(l1_hit == false && vc_hit == true){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

		/* Swap blocks between L1 and VC */
		for (int i = 0; i < l1_vc_info->vc_assoc; i++)
		{
			if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag)
			{
				/* corresponding tag on VC is set to evicted adress value from L1 */
				vc_cache_blocks[i].tag = l1_result->evicted_address;
			}
		}
		
		/* if LOAD */
        if(loadstore == 0){
        	vc_result->miss_hit = HIT_LOAD;
        }

        /* if STORE */
		else{
        	vc_result->miss_hit = HIT_STORE;
        }
	}

	/* In case of miss on L1 and VC*/
	if(l1_hit == false && vc_hit == false){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

		/* Set dirty eviction true if dirty bit of last block (to be evicted) is 1*/
		if(vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty == true){
           vc_result->dirty_eviction = true;
        }
        else{
           vc_result->dirty_eviction = false;
        }

		/*Evicted address set as tag of last (rightmost) block*/
		vc_result->evicted_address = vc_cache_blocks[l1_vc_info->vc_assoc-1].tag;

        /* if LOAD */
        if(!loadstore){
			/* No need to set dirty bit in LOAD scenarios */
            vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty = false;
            vc_result->miss_hit = MISS_LOAD;
        }

        /* if STORE */
        else{
			/* Dirty bit is set in STORE scenarios */
            vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty = true;
            vc_result->miss_hit = MISS_STORE;
        }

		/* If there were no hits in either cache, and VC is active, FIFO must be shifted to accomodate evicted block from L1*/
		if (l1_full == true)
		{
			/* Shifting: */					
			int temp = vc_cache_blocks[0].tag;
			bool temp_valid = vc_cache_blocks[0].valid;
			bool temp_dirty = vc_cache_blocks[0].dirty;
			for (int i = 0; i < l1_vc_info->vc_assoc-1; i++)
			{	
				int aux = vc_cache_blocks[i+1].tag;
				bool aux_valid = vc_cache_blocks[i+1].valid;
				bool aux_dirty = vc_cache_blocks[i+1].dirty;
				vc_cache_blocks[i+1].tag = temp;
				vc_cache_blocks[i+1].valid = temp_valid;
				vc_cache_blocks[i+1].dirty = temp_dirty;
				temp = aux;
				temp_valid = aux_valid;
				temp_dirty = aux_dirty;
			}

			/* First block in VC is now the most recently evicted L1 block */
			vc_cache_blocks[0].tag = temp;
			vc_cache_blocks[0].valid = temp_valid;
			vc_cache_blocks[0].dirty = temp_dirty;
			vc_cache_blocks[0].tag = l1_result->evicted_address;
			
		}
		
	}
   	
   	return OK;
}