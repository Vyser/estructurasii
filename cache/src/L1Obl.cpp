/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_obl_replacement_policy(int idx,
                               int tag,
                               int associativity,
                               bool loadstore,
                               entry *cache_block,
                               entry *cache_block_obl,
                               operation_result *operation_result_cache_block,
                               operation_result *operation_result_cache_obl,
                               bool debug = false)
{
    /* OBL FLAGS */

    /*If tag has OBL bit set, this is set*/
    bool hit_obl = false;
    /*If a prefetch is requiered, this is set. cleared if no prefetch is requiered*/
    bool need_prefetch = true;
    /*If block n must be fetched (L1 miss for block n), this is set (initialized as true for simplicity, and cleared accordingly)*/
    bool L1_miss = true;

    /* Iterate through L1 */
    for (int i = 0; i < associativity; i++)
    {
        /* Check if block n is in L1 */
        if ((cache_block[i].tag == tag) && (cache_block[i].valid == 1))
        {
            /* In case of L1 hit on block n, no need to fetch n*/
            L1_miss = false;
            /* Check if block n was demand-fetched/had obl_tag cleared or recently prefetched */
            if (cache_block[i].obl_tag == 1)
            {
                /* If block n was prefetched, OBL tag bit is cleared and an OBL hit is registered*/
                hit_obl = true;
                cache_block[i].obl_tag = 0;
            }
            if (debug)
            {
                cout << "El bloque n es un hit en L1" << endl;
            }
            /* LRU is called */
            lru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_block,
                                   operation_result_cache_block,
                                   debug);
            break;
        }
    }

    /* In case of an OBL hit (block n was prefetched) */
    if (hit_obl == true)
    {
        if (debug)
        {
            cout << "El bloque n produjo un OBL hit" << endl;
        }

        /* Check if block n+1 is in L1 already */
        for (int i = 0; i < associativity; i++)
        {
            if (cache_block_obl[i].tag == tag)
            {
                /* If block n+1 is already in cache, no need for prefetch */
                need_prefetch = false;
                if (debug)
                {
                    cout << "El bloque n+1 ya se encuentra en L1, no se debe realizar prefetch" << endl;
                }
                break;
            }
        }

        /* LRU is called */
        lru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_block,
                               operation_result_cache_block,
                               debug);

        /* If n+1 is not in the cache yet, it is fetched (prefetch)*/
        if (need_prefetch == true)
        {
            if (debug)
            {
                cout << "El bloque n + 1 no se encuentra en L1, se debe realizar prefetch" << endl;
            }

            /* n+1 block is loaded into L1, keeping dirty bit value untouched*/
            for (int i = 0; i < associativity; i++)
            {
                /* We want to load the next block, so we check rp_value */
                if (cache_block_obl[i].rp_value == 0)
                {
                    /* if dirty bit is 1, a dirty eviction was made */
                    if (cache_block_obl[i].dirty == true)
                    {
                        operation_result_cache_obl->dirty_eviction = true;
                    }
                    else
                    {
                        operation_result_cache_obl->dirty_eviction = false;
                    }
                    /* evicted adress is stored */
                    operation_result_cache_obl->evicted_address = cache_block_obl[i].tag;

                    /* if LOAD */
                    if (!loadstore)
                    {
                        cache_block_obl[i].dirty = false;
                        operation_result_cache_obl->miss_hit = MISS_LOAD;
                    }

                    /* if STORE */
                    else
                    {
                        operation_result_cache_obl->miss_hit = MISS_STORE;
                    }

                    /* rp_values are updated */
                    for (int j = 0; j < associativity; j++)
                    {
                        if (cache_block_obl[j].rp_value > cache_block_obl[i].rp_value)
                        {
                            cache_block_obl[j].rp_value--;
                        }
                    }

                    /* n+1 block is set as the MRU, since it was just prefetched */
                    cache_block_obl[i].rp_value = associativity - 1;

                    /* obl_tag for n+1 block is set (1), since it was just prefetched*/
                    cache_block_obl[i].obl_tag = 1;
                    cache_block_obl[i].tag = tag;
                    cache_block_obl[i].valid = true;
                    break;
                }
            }
        }
    }

    /* If block n is not in L1 cache */
    if (L1_miss == true)
    {
        if (debug)
        {
            cout << "El bloque n no esta en el cache, se realiza un fetch" << endl;
        }

        /* LRU is called*/
        lru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_block,
                               operation_result_cache_block,
                               debug);

        /* Check for n+1 in L1 cache */
        for (int i = 0; i < associativity; i++)
        {
            if (cache_block_obl[i].tag == tag)
            {
                need_prefetch = false;
                if (debug)
                {
                    cout << "El bloque n+1 ya se encuentra en el cache, no se hace prefetch" << endl;
                }
                break;
            }
        }

        /* if n+1 is not in L1, it is prefetched */
        if (need_prefetch == true)
        {
            for (int i = 0; i < associativity; i++)
            {
                if (cache_block_obl[i].rp_value == LRU)
                {

                    /* Insert block n+1 as a LRU miss but keep dirty bit untouched*/
                    if (cache_block_obl[i].dirty == true)
                    {
                        operation_result_cache_obl->dirty_eviction = true;
                    }
                    else
                    {
                        operation_result_cache_obl->dirty_eviction = false;
                    }
                    operation_result_cache_obl->evicted_address = cache_block_obl[i].tag;

                    /* if LOAD */
                    if (!loadstore)
                    {
                        cache_block_obl[i].dirty = false;
                        operation_result_cache_obl->miss_hit = MISS_LOAD;
                    }

                    /* if STORE */
                    else
                    {
                        operation_result_cache_obl->miss_hit = MISS_STORE;
                    }

                    /* rp_values updated */
                    for (int j = 0; j < associativity; j++)
                    {
                        if (cache_block_obl[j].rp_value > cache_block_obl[i].rp_value)
                        {
                            cache_block_obl[j].rp_value--;
                        }
                    }

                    /* n+1 block is set as MRU, since it was just prefetched */
                    cache_block_obl[i].rp_value = associativity - 1;

                    /* obl_tag is set, since n+1 block was just prefetched*/
                    cache_block_obl[i].obl_tag = 1;
                    cache_block_obl[i].tag = tag;
                    cache_block_obl[i].valid = true;
                    break;
                }
            }
        }
    }

    return OK;
}
