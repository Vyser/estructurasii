/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>

class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on, Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on, Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 1;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result result = {};
  int policy;

  /* Choose a random policy */
  policy = rand()% 3 + 1;
  if(debug_on){
    printf("Replacement Policy: ");
    if(policy == 1){
      printf("LRU\n");
    }
    else if(policy == 2){
      printf("NRU\n");
    }
    else if(policy == 3){
      printf("SRRIP\n");
    }
  }

  /* Choose a random associativity */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  int M = 0;
  if(associativity <= 2){
    M = 1;
  }
  else{
    M = 2;
  }
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity];

  /* Fill a cache entry */
  for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
      if(policy == 1){
        cache_line[i].rp_value = i;
      }
      else if((policy == 2) || (policy == 3 && (M == 1))){
        cache_line[i].rp_value = 1;
      }
      else if((policy == 3) && (M == 2)){
        cache_line[i].rp_value = (associativity <= 2) ? rand()%associativity : 3;
      }
  }
  if(debug){
    printf("Before inserting a new block A\n");
    for(int i = 0; i< associativity; i++){
      printf("cache_line.tag: %d\n", cache_line[i].tag);
    }
  }  

  /* Insert a new block A */
  /* Force a hit on A: block will be a miss then, if we use the same 
   * tag again, we will get a hit */
  struct entry block_A[1];
  block_A[0].valid = true;
  block_A[0].tag = 256;
  block_A[0].dirty = 0;
  if(policy == 1){
    block_A[0].rp_value = 0;
  }
  else if((policy == 2) || (policy == 3 && (M == 1))){
    block_A[0].rp_value = 1;
  }
  else if((policy == 3) && (M == 2)){
    block_A[0].rp_value = 3;
  }
  
  
  
  DEBUG(debug_on, Checking block A hit operation);
  for (i = 0 ; i < 2; i++){
    if(policy == 1){
      status = lru_replacement_policy(idx,
                                        block_A[0].tag,
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        bool(debug_on));
    }
      else if(policy == 2){
      status = nru_replacement_policy(idx,
                                        block_A[0].tag,
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        bool(debug_on));
    }
      else if(policy == 3){
      status = srrip_replacement_policy(idx,
                                        block_A[0].tag,
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        bool(debug_on));
    }
  }

  /* Check rp_value of block A */
  int rp_val_ = (int)cache_line[0].rp_value;
  int rp_value_lru = associativity - 1;
  if(policy == 1){
    EXPECT_EQ(rp_val_, rp_value_lru);
  }
  else{
    EXPECT_EQ(rp_val_, 0);
  }


  /* Keep inserting new blocks until A is evicted */
  /* Check eviction of block A happen after N new blocks were inserted */
  /* (where N depends of the number of ways) */

  int N = 0;
  int compare = 0;
  int entered[associativity];
  if((policy == 3) && (M == 2)){
    while(result.evicted_address != block_A[0].tag){
      N++;
      tag = rand() % 500000 + 5100;
      status = srrip_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line,
                                          &result,
                                          bool(debug_on));
    } 
    if(result.evicted_address == block_A[0].tag){
        EXPECT_EQ(result.evicted_address, block_A[0].tag);
        compare = N;
    } 
  }
  else{
    for (N = 0 ; N < associativity; N++){
      tag = rand() % 500000 + 5100;

      if(policy == 1){
        status = lru_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line,
                                          &result,
                                          bool(debug_on));
      }
      else if(policy == 2){
        status = nru_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line,
                                          &result,
                                          bool(debug_on));
      }
      else if((policy == 3) && (M == 1)){
        status = srrip_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line,
                                          &result,
                                          bool(debug_on));                               
      }   
      if(result.evicted_address == block_A[0].tag){
        EXPECT_EQ(result.evicted_address, block_A[0].tag);
        compare = N;
      }
    }
  }
  if((policy == 3) && (M == 2)){
    printf("For SRRIP with M = 2 the evicted address is not necessarily the last spot");
  }
  else{
    EXPECT_EQ(associativity - 1, compare);
  }
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result result = {};
  int policy;

  /* Choose a random policy */
  policy = rand()%3 + 1;
  if(debug_on){
    printf("Replacement Policy: ");
    if(policy == 1){
      printf("LRU\n");
    }
    else if(policy == 2){
      printf("NRU\n");
    }
    else if(policy == 3){
      printf("SRRIP\n");
    }
  }

  /* Choose a random associativity */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  //associativity = 8;
  int M = 0;
  if(associativity <= 2){
    M = 1;
  }
  else{
    M = 2;
  }
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[2][associativity];

  /* Fill a cache entry with only read operations*/
  for(int i = 0; i < 2; i++){
    for (int j =  0; j < associativity; j++) {
      cache_line[i][j].valid = true;
      cache_line[i][j].tag = rand()%4096;
      cache_line[i][j].dirty = 0;
      while (cache_line[i][j].tag == tag) {
        cache_line[i][j].tag = rand()%4096;
      }
      if(policy == 1){
        cache_line[i][j].rp_value = j;
      }
      else if((policy == 2) || (policy == 3 && (M == 1))){
        cache_line[i][j].rp_value = 1;
      }
      else if((policy == 3) && (M == 2)){
        cache_line[i][j].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
      if(debug){
        printf("Index: %d, tag: %d, rp_value: %d\n", i, cache_line[i][j].tag, cache_line[i][j].rp_value);
        printf("\n");
      }
    }
  }

  int entered[associativity];
  entered[0] = tag;


  for(int i = 0; i < 2; i++){
    for (int j = 0 ; j < associativity; j++){
      entered[j] = tag + j;
      if(policy == 1){
        status = lru_replacement_policy(idx,
                                          entered[j],
                                          associativity,
                                          loadstore,
                                          cache_line[i],
                                          &result,
                                          bool(debug_on));                                
      }
      else if(policy == 2){
        status = nru_replacement_policy(idx,
                                          entered[j],
                                          associativity,
                                          loadstore,
                                          cache_line[i],
                                          &result,
                                          bool(debug_on));
      }
      else if(policy == 3){
        status = srrip_replacement_policy(idx,
                                          entered[j],
                                          associativity,
                                          loadstore,
                                          cache_line[i],
                                          &result,
                                          bool(debug_on));
      }
    }
  }
  if(debug){       
    for(int j = 0; j < associativity; j++){
      printf("entered: %d\n", entered[j]);
    }
  }  
  

  /* Force a write hit for a random block A */
  
  int tag_for_hit = rand()%associativity;
  int block_A = entered[tag_for_hit];
  if(debug){
    printf("forced write hit of: %d in block A\n", block_A);
  }  
  loadstore = 1;

  if(policy == 1){
    status = lru_replacement_policy(idx,
                                      entered[tag_for_hit],
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == 2){
    status = nru_replacement_policy(idx,
                                      entered[tag_for_hit],
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == 3){
    status = srrip_replacement_policy(idx,
                                      entered[tag_for_hit],
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  


  /* Force a read hit for a random block B */
  loadstore = 0;
  tag_for_hit = rand()%associativity;
  int block_B = entered[tag_for_hit];
  if(debug){
    printf("forced read hit of: %d in block B\n", block_B);
  }  
  if(policy == 1){
    status = lru_replacement_policy(idx,
                                      block_B,
                                      associativity,
                                      loadstore,
                                      cache_line[1],
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == 2){
    status = nru_replacement_policy(idx,
                                      block_B,
                                      associativity,
                                      loadstore,
                                      cache_line[1],
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == 3){
    status = srrip_replacement_policy(idx,
                                      block_B,
                                      associativity,
                                      loadstore,
                                      cache_line[1],
                                      &result,
                                      bool(debug_on));
  }

  /* Force read hit for random block A */
  loadstore = 0;
  if(debug){
    printf("forced read hit of: %d in block A\n", block_A);
  }  
  if(policy == 1){
    status = lru_replacement_policy(idx,
                                      block_A,
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == 2){
    status = nru_replacement_policy(idx,
                                      block_A,
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == 3){
    status = srrip_replacement_policy(idx,
                                      block_A,
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  if(debug){ 
    printf("until here parts 4-6\n");
    for(int i = 0; i < 2; i++){
      for(int j = 0; j <associativity; j++){
        printf("Index: %d, tag: %d, rp_value: %d. Dirty: %d\n", i, cache_line[i][j].tag, cache_line[i][j].rp_value, cache_line[i][j].dirty);
        printf("\n");
      }
      
    }
  }

  /* Insert lines until A & B are evicted */
  tag = rand() % 250000 + 350000;
  int compare = 0;
  entered[0] = tag;
  int N = 0;

  /* CASE SSRIP, M = 2 */
  if((policy == 3) && (M == 2)){
    while(result.evicted_address != block_B){
      N++;
      if(N == associativity){
        N = 0;
      }
      tag = rand() % 5200 + 5000;
      status = srrip_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line[1],
                                          &result,
                                          bool(debug_on));
    }                                             
    if((result.evicted_address == block_B) && (result.dirty_eviction == false)){
            //printf("entra el block B al if evicted: %d. \n", block_B);
            EXPECT_EQ(result.evicted_address, block_B);
            compare = N;
            EXPECT_EQ(result.dirty_eviction, false);
    }
    N = 0;
    while(result.evicted_address != block_A){
      N++;
      if(N == associativity){
        N = 0;
      }
      tag = rand() % 5200 + 5000;
      status = srrip_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line[0],
                                          &result,
                                          bool(debug_on));
    }                                             
    if((result.evicted_address == block_A) && (result.dirty_eviction == true)){
      EXPECT_EQ(result.evicted_address, block_A);
      compare = N;
      EXPECT_EQ(result.dirty_eviction, true);
    }    
  }

  /* CASE !SSRIP, M = 1 */
  else{
    for(int i = 0; i < 2; i++){
      for (int j = 0 ; j < associativity; j++){
        entered[j] = tag + j + i*2000;
        if(policy == 1){
          status = lru_replacement_policy(idx,
                                            entered[j],
                                            associativity,
                                            loadstore,
                                            cache_line[i],
                                            &result,
                                            bool(debug_on));                                
        }
        else if((policy == 2) || (policy == 3 && (M == 1))){
          status = nru_replacement_policy(idx,
                                            entered[j],
                                            associativity,
                                            loadstore,
                                            cache_line[i],
                                            &result,
                                            bool(debug_on));
        }
        if(i == 0){
          if(result.evicted_address == block_A){
            /* Enters block A if evicted, compares with cache_line[i][j].tag */
            EXPECT_EQ(result.evicted_address, block_A);
            compare = j;
            
            EXPECT_EQ(result.dirty_eviction, true);
          }
        } 
        if(i == 1){ 
          if(result.evicted_address == block_B){
            /* Enters block B if evicted, compares with cache_line[i][j].tag */
            EXPECT_EQ(result.evicted_address, block_B);
            compare = j;
            
            EXPECT_EQ(result.dirty_eviction, false);
          }
        }  
      }
    }
  }  
}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  bool loadstore = 1;
  bool debug = 0;
  int policy = 0;
  int M = 0;
  struct operation_result result = {};

  /* Choose a random policy */
  policy = rand() % 3 + 1;

  /* Choose invalid parameters for idx, tag and asociativy */
  idx = -1 * (rand()%1024);
  tag = -1 * (rand()%4096);
  associativity = -1 * (1 << (rand()%4));
  if((-1 * associativity) <= 2){
    M = 1;
  }
  else{
    M = 2;
  }
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  /* Fill a cache entry */
  struct entry cache_line[-1 * associativity];
  for ( i =  0; i < (-1 * associativity); i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
      if(policy == 1){
        cache_line[i].rp_value = i;
      }
      else if((policy == 2) || (policy == 3 && (M == 1))){
        cache_line[i].rp_value = 1;
      }
      else if((policy == 3) && (M == 2)){
        cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
  }

  /* Check function returns a PARAM error condition */
  if(policy == 1){
    status = lru_replacement_policy(idx,
                                      tag,
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == 2){
    status = nru_replacement_policy(idx,
                                      tag,
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == 3){
    status = srrip_replacement_policy(idx,
                                      tag,
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));
  }


  EXPECT_EQ(status, 1);
}
