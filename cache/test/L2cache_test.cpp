/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include "debug_utilities.h"
#include "L1cache.h"
#include "L2cache.h"

using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};


/*
 * TEST3:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 5. Se llena L1 al producir accesos distintos que A
 * 6. Se genera un miss en L1 y L2 al accesar A
 * 7. Se accesa A para producir un hit en L1 y L2
 * 8. Se verifica que hay hit en L1 y L2
 */
TEST_F(L2cache,l1_hit_l2_hit){
	EXPECT_TRUE(true);
}



/*
 * TEST4:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 al producir accesos distintos que A.
 * 6. Se genera un miss en L1 y hit en L2 al accesar A.
 */
TEST_F(L2cache,l1_miss_l2_hit){
	EXPECT_TRUE(true);
}


/*
 * TEST5:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 y L2 al producir accesos distintos que A.
 * 6. Se genera un miss en L1 y en L2 al accesar A.
 */
TEST_F(L2cache,l1_miss_l2_miss){
	EXPECT_TRUE(true);
}
