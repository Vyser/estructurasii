#include "matrix.h"

/* 
* Input: int n
* Output: matriz int nxn con numeros aleatorios entre 0 y 100
* Esta funcion toma un entero n y retorna una matriz nxn llena con enteros aleatorios entre 0 y 100
*/
int** generateSquareMatrix(int n){
    srand ( time(NULL) ); // semilla aleatoria
    int** matrix;
    // Asignacion de memoria
    matrix = malloc(n*sizeof(int*));
    for (int i = 0; i < n; i++){
        matrix[i] = malloc(n*sizeof(int));
    }

    // Se llena la matriz con enteros aleatorios
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            matrix[i][j] = rand()%101;
        }
    }
    return matrix;
}

/* 
* Input: int n
* Output: vector int nx1 con entradas aleatorias
* Esta funcion toma un entero n y retorna un vector nx1 lleno con enteros aleatorios
*/
int* generateVector(int n){
    srand( (time(NULL)) ); // semilla aleatoria
    int* vector;
    // Asignacion de memoria
    vector = malloc(n*sizeof(int));

    // Se llena el vector con enteros aleatorios
    for (int i = 0; i < n; i++){
        vector[i] = (int) rand()%101;
    }
    return vector;
}

/*
* Funcion de utilidad para imprimir el contenido de una matriz nxn
*/
void printMatrix(int** matrix, int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            printf("%3d ", matrix[i][j]);
            if (j == n-1){
                printf("\n");
            }
        }
    }
}

/*
* Funcion de utilidad para imprimir el contenido de una matriz nx1
*/
void printVector(int* vector, int n){
    for (int i = 0; i < n; i++){
        printf("%3d\n", vector[i]);
    }
}

/*
* Funcion de utilidad para liberar la memoria asignada a una matriz
*/
void deleteMatrix(int** matrix, int n){
    for (int i = 0; i < n; i++){
        free(matrix[i]);
    }
    free(matrix);
}

/*
* Funcion de utilidad para liberar memoria asignada a un vector
*/
void deleteVector(int* vector){
    free(vector);
}

/*
* Input: matriz int nxn, vector int nxn, int n
* Output: vector resultante de la matriz por el vector
* Esta funcion implementa la multiplicacion de una matriz nxn por un vector nx1
*/
int* serialMultiply(int** matrix, int* vector, int n){
    int* result = calloc(n, sizeof(int));

    for (int i = 0; i < n; i++){
        // Algoritmo de calculo de cada entrada del vector resultante
        for (int j = 0; j < n; j++){
            result[i] += matrix[i][j] * vector[j];
        }
    }
    return result;
}

/************************************* LA VERSION PARALELA IMPLEMENTADA EN MAIN.C *****************************************/

/*
* Funcion de utilidad para comparar dos vectores. Retorna 1 si son iguales, 0 si son diferentes
*/
int compareVector(int* result1, int* result2, int n){
    for (int i = 0; i < n; i++){
        if (result1[i] != result2[i]){
            return 0;
        }
    }
    return 1;
}