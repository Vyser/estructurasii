#include "matrix.h"
#include "pi.h"

int** matrix; // Matriz para multiplicacion
int* vector; // Vector para multiplicacion
int* paral_result; // Vector donde se guarda el resultado de la multiplicacion paralela
int thread_quant, n; // Variables para ver la cantidad de filas e hilos

/*
* Input: posicion del vector donde se esta obteniendo el valor de la multiplicacion
* Esta funcion toma la posicion del vector con el que se esta obteniendo el valor de la multiplicacion y modifica
* paral_result para guardar el resultado en la fila correspondiente. Esta funcion realiza los calculos de una fila
*/
void* parallelrow(void* arguments){
    int *p = (int*)arguments;
    int pos = *p;
    for (int j = 0; j < n; j++){
        paral_result[pos] += matrix[pos][j] * vector[j];
    }
}

int main(int argc, char* argv[]){
    srand(time(NULL)); // Semilla aleatoria
    n = rand()%6 + 3; // El tamaño de las matrices y vectores esta definido para 3 < n < 8

    // MATRIX

    // SERIAL
    matrix = generateSquareMatrix(n);
    vector = generateVector(n);
    printf("La matriz de la multiplicación es: \n");
    printMatrix(matrix, n);
    printf("El vector de la multiplicación es: \n");
    printVector(vector, n);
    printf("El resultado de la multiplicación serial es: \n");
    int* result = serialMultiply(matrix, vector, n);
    printVector(result, n);

    // PARALLEL
    thread_quant = n;
    paral_result = calloc(n, sizeof(int)); // Se asigna la memoria asociada a paral_result
    pthread_t threads[thread_quant]; // Creacion de los hilos
    int result_code; // Para asserts
    int tNum[thread_quant]; // Se utiliza este vector para evitar segmentation faults, cada numero estara en una posicion de memoria diferente
    
    // Inicializacion de los trabajos en cada hilo
    for (int i = 0; i < thread_quant; i++){
        tNum[i] = i;
        result_code = pthread_create(&threads[i], NULL, parallelrow, &tNum[i]);
        assert(!result_code);
    }
    // JOIN de los hilos
    for (int i = 0; i < thread_quant; i++){
        result_code = pthread_join(threads[i], NULL);
        assert(!result_code);
    }
    printf("El resultado de la multiplicación paralela es: \n");
    printVector(paral_result, n);
    int compare = compareVector(result, paral_result, n);
    deleteMatrix(matrix, n);
    deleteVector(vector);
    deleteVector(paral_result);
    deleteVector(result);


    // PI
    double serial_time = 0.0;
    double paralel_time = 0.0;

    clock_t begin_serial = clock();
    float serial_result = serialPi();
    clock_t end_serial = clock();
    serial_time += (double)(end_serial-begin_serial)/CLOCKS_PER_SEC;


    clock_t begin = clock();
    float paralel_result = paralelPi();
    clock_t end = clock();
    paralel_time += (double)(end-begin)/CLOCKS_PER_SEC;
    printf("\n\nEstimaciones del número Pi:\n");
    printf("Serial:   %.20lf\n",serial_result);
    printf("Paralelo: %.20lf\n",paralel_result);

    if (compare && (paralel_result == serial_result)){
        return 0;
    }
    return -1;
}
