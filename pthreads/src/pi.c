#include "pi.h"
#define AVAILABLE_THREADS 8 //Intel Core i5 i5-8265U: 4 cores 8 threads
#define TERMS_TO_APPROX 15 // cantidad de terminos para la aproximacion, definido arbitrariamente

float sum = 1;
pthread_mutex_t mutexsum;

float serialPi (){ //Para la estimación de forma serial de Pi (un solo thread)
    sum = 1;
    for (int i = 1; i <= TERMS_TO_APPROX; i++){
        sum += pow(-1.0,i)*(1.0/((2.0*i+1.0)*pow(3.0,i)));
    }
    return sqrt(12)*sum;
}

float paralelPi(){ //Para la estimación de Pi haciendo uso de todos los threads disponibles
    sum = 1;
    pthread_t threads[AVAILABLE_THREADS];
    int tNum[AVAILABLE_THREADS];
    int code_result;

    for (int i = 0; i < AVAILABLE_THREADS; i++){
        tNum[i] = i;
        code_result = pthread_create(&threads[i], NULL, threadRoutine,&tNum[i]);
        assert(!code_result);
    }
    for (int i = 0; i < AVAILABLE_THREADS; i++){
        code_result = pthread_join(threads[i], NULL);
        assert(!code_result);
    }
    return sqrt(12)*sum;
}

void *threadRoutine(void *vargp){
    int* that_thread = (int*) vargp;
    int this_thread = *that_thread;
    int first_term = 0;
    int last_term = 0;
    int terms_per_thread = TERMS_TO_APPROX/AVAILABLE_THREADS;
    bool loaded_thread = false;

    if (this_thread <= TERMS_TO_APPROX%AVAILABLE_THREADS){
        loaded_thread = true;
    }
    else
    {
        loaded_thread = false;
    }
    

    if (loaded_thread){
        first_term = (terms_per_thread+1)*this_thread + 1;
        if (this_thread == TERMS_TO_APPROX%AVAILABLE_THREADS){
            last_term = ((this_thread+1)*terms_per_thread)+(this_thread);
        }
        else
        {
            last_term = ((this_thread+1)*terms_per_thread)+(this_thread) + 1;
        }
    }
    else
    {
        first_term = 1 + terms_per_thread*this_thread + TERMS_TO_APPROX%AVAILABLE_THREADS;
        last_term = first_term + terms_per_thread - 1;
    }


    pthread_mutex_lock (&mutexsum);
    for (int i = first_term; i <= last_term; i++){
        sum += pow(-1.0,i)*(1.0/((2.0*i+1.0)*pow(3.0,i)));
    }
    pthread_mutex_unlock (&mutexsum);

}