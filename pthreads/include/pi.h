#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h> 

float serialPi ();

float paralelPi ();

void *threadRoutine(void *vargp);
