#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <assert.h>
#include <unistd.h>

int** generateSquareMatrix(int n);

int* generateVector(int n);

void printMatrix(int** matrix, int n);

void printVector(int* vector, int n);

void deleteMatrix(int** matrix, int n);

void deleteVector(int* vector);

int* serialMultiply(int** matrix, int* vector, int n);

int compareVector(int* result1, int* result2, int n);