# Computer Structures II

Repository with the programming homeworks done for the course Computer Structures II, from the University of Costa Rica. Each directory holds its own README and related code to solve each homework problem. The main focus of the course is to understand a computer's Cache memory and memory hierarchy. All homeworks done using C or C++.
