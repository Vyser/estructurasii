#include <string>
#include <iostream>
#include <cstring>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;

int get_bits(string dir, int s);

int get_bits(int dir, int s);

void print_table(string type, int s, int ph, int gh, int branches, int correct_taken, int incorrect_taken, int correct_not, int incorrect_not);