#include "utility_funcs.h"

vector<int> bimodal(int s);

vector<int> pshare(int s, int ph);

vector<int> gshare(int s, int gh);

vector<int> tournament(int s, int ph, int gh);