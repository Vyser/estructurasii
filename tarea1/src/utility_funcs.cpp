#include "utility_funcs.h" // Si sale un warning, no importa, en el Makefile se soluciona

/*
Recorta los bits de un número. Toma un string que representa una dirección de memoria y los s bits que se desean del mismo y retorna
los últimos s bits de la dirección de memoria
*/
int get_bits(string dir, int s){
    long bitMask = 0; // Definición de una máscara para recortar bits
    long num = stol(dir); // Se convierte la dirección de memoria a long
    // Se prepara la máscara para hacer un bitwise and en las posiciones de los bits que se desean
    while (s > 0){
        bitMask = bitMask << 1;// left logical shift
        bitMask++; // +1
        s--;
    }
    return (num & bitMask) ; // Se retornan los últimos s bits de dir
}

/*
Recorta los bits de un número. Igual a la función anterior, solo que se sobrecarga para que pueda trabajar con variables int
*/
int get_bits(int dir, int s){
    long bitMask = 0;
    while (s > 0){
        bitMask = bitMask << 1;
        bitMask++;
        s--;
    }
    return (dir & bitMask) ;
}

/* 
Función encargada de imprimir en la terminal la tabla con los resultados finales. Recibe el tipo de predictor (type), s,
el tamaño de los registros de historia privada (ph), el tamaño del registro de historia global (gh), la cantidad de saltos correctos
tomados (correct_taken) y no tomados (correct_not), la cantidad de saltos incorrectos tomados (incorrect_taken)
y no tomados (incorrect_not), y la cantidad total de saltos (branches). No tiene valor de retorno.
*/
void print_table(string type, int s, int ph, int gh, int branches, int correct_taken, int incorrect_taken,\
    int correct_not, int incorrect_not){
    int entrySize = pow(2,s); // Tamaño de una entrada de BHT
    double percentage = ((correct_taken + correct_not)*100.0) / (branches); //Se obtiene el porcentage de saltos correctos

    cout << fixed;
    cout << setprecision(2);
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
    cout << "Prediction parameters:" << endl;
    cout << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
    cout << "Branch prediction type: " << type << endl;
    cout << endl;
    cout << "BHT size (entries): " << entrySize << endl;
    cout << endl;
    cout << "Global history register size: " << gh << endl;
    cout << endl;
    cout << "Private history register size: " << ph <<endl;
    cout << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
    cout << "Simulation results:" << endl;
    cout << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
    cout << "Number of branches: " << branches << endl;
    cout << endl;
    cout << "Number of correct predictions of taken branches: " << correct_taken << endl;
    cout << endl;
    cout << "Number of incorrect predictions of taken branches: " << incorrect_taken << endl;
    cout << endl;
    cout << "Number of correct predictions of not-taken branches: " << correct_not << endl;
    cout << endl;
    cout << "Number of incorrect predictions of not-taken branches: " << incorrect_not << endl;
    cout << endl;
    cout << "Percentage of correct predictions: " << percentage << endl;
    cout << endl;
    cout << "------------------------------------------------------------------------" << endl;
}