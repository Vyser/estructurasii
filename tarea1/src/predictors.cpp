#include "predictors.h" // Si sale un warning, no importa, en el Makefile se soluciona

/*
Implementación del predictor bimodal. Toma como entrada la cantidad de bits s que se tomarán del PC y retorna un vector de enteros
con los saltos totales realizados, "taken" correctos, "taken" incorrectos, "not taken" correctos y "not taken" incorrectos.
Lee los PC del archivo proveído por medio de un while con cin. Se manejan los índices y contenidos de las tablas con variables de tipo int,
para facilitar el uso de operaciones "bitwise" de C++ y manejo numérico en general, pero la estructura en general obedece los patrones
que describen el algoritmo en base binaria.
*/
vector<int> bimodal(int s){
    /* total_indices es la cantidad de entradas que tendrá el arreglo, index es una variable a utilizar para iterar sobre los elementos
    del arreglo, branches es la variable que indicará la cantidad total de saltos realizados, correct_* indica los taken o not taken
    correctos según corresponda, mientras que incorrect_* indica los taken o not taken incorrectos según corresponda*/
    int total_indices = pow(2,s), index = 0, branches = 0;
    int correct_taken = 0, correct_not = 0;
    int incorrect_taken = 0, incorrect_not = 0;

    /* PC indica el program counter que se está leyendo del documento, prediction es la predicción que realiza el predictor,
    documented_outcome es el resultado que está registrado en el documento, states es el arreglo que almacenará a todos los 2bc
    del predictor. La función fill preinicializa los contenidos del arreglo al estado Strongly Not Taken (SN), con un valor de 0*/
    string PC = "";
    char prediction, documented_outcome;
    int states [ total_indices ];
    fill(states, states + total_indices, 0);

    // Mientras aún hay PCs por leer en el documento
    while (cin >> PC){
        // En cada iteración se entra a un branch, por lo que se le suma 1 a la cantidad total de branches y se calcula el índice con el PC
        branches++;
        index = get_bits(PC, s); // Cálculo de índice en el arreglo

        // Si la predicción tiene un 0 en el bit más significativo, es un not taken. Caso contrario es un taken
        prediction = (states[index] < 2) ? 'N':'T';

        // Se carga el resultado del documento a la variable documented_outcome
        cin >> documented_outcome;

        // Un switch statement que revisa si las predicciones coinciden con los resultados del documento.
        switch (prediction){
            // Si se predice un not taken
            case 'N':
                /* Si el documento tenía un taken, el not taken predicho es incorrecto, entonces se incrementa el 2bc correspondiente
                del arreglo y se suma 1 a la cuenta de not taken incorrectos. En este caso no hace falta revisar si el 2bc es mayor
                o menor a algún valor dado que el primer bit es 0 y se le suma 1*/
                if (documented_outcome == 'T'){
                    states[index]++;
                    incorrect_taken++;
                } // Caso contrario, se le resta 1 al 2bc (si este no se encuentra en strongly not taken) y se suma 1 a los not taken correctos
                else if (documented_outcome == 'N'){
                    correct_not++;
                    if (states[index] > 0){
                        states[index]--;
                    }
                }
                break;
            // Similar al caso 'N'. Se contabilizan los taken correctos y se manejan los 2bc correspondientes al caso.
            case 'T':
                if (documented_outcome == 'N'){
                    states[index]--;
                    incorrect_not++;
                }
                else if (documented_outcome == 'T'){
                    correct_taken++;
                    if (states[index] < 3 ){
                        states[index]++;
                    }
                }
                break;
        }
    }
    // Se retornan todas las variables deseadas.
    int returningVals [5] = {branches, correct_taken, incorrect_taken, correct_not, incorrect_not};
    return vector<int> (returningVals, returningVals + sizeof(returningVals)/sizeof(int) );
}

/*
Implementación del predictor Pshare. Toma como entradas la cantidad de bits a tomar del PC (s) y la cantidad de bits de los registros de 
historia (ph). Retorna un vector de enteros con los saltos totales realizados, "taken" correctos, "taken" incorrectos, "not taken"
correctos y "not taken" incorrectos. Lee los PC del archivo proveído por medio de un while con cin. Se manejan los índices y contenidos 
de las tablas con variables de tipo int, para facilitar el uso de operaciones "bitwise" de C++ y manejo numérico en general, 
pero la estructura en general obedece los patrones que describen el algoritmo en base binaria.
*/
vector<int> pshare(int s, int ph){
    /* total_indices es la cantidad de entradas que tendrán el PHT y el BHT, los index son variables a utilizar para iterar sobre los elementos
    del PHT y BHT, branches es la variable que indicará la cantidad total de saltos realizados, correct_* indica los taken o not taken
    correctos según corresponda, mientras que incorrect_* indica los taken o not taken incorrectos según corresponda*/
    int total_indices = pow(2,s), pht_index = 0, bht_index = 0;
    int branches = 0, correct_taken = 0, correct_not = 0;
    int incorrect_taken = 0, incorrect_not = 0;

    /* PC indica el program counter que se está leyendo del documento, prediction es la predicción que realiza el predictor,
    documented_outcome es el resultado que está registrado en el documento, states es el arreglo que almacenará a todos los 2bc
    del predictor y patterns es un arreglo que representa el PHT. Se manejarán con enteros, con conciencia de su representación binaria
    La función fill preinicializa los contenidos del arreglo al estado Strongly Not Taken (SN), con un valor de 0*/
    string PC = "";
    char prediction, documented_outcome;
    int patterns [ total_indices ];
    int states [ total_indices ];
    fill(patterns, patterns + total_indices, 0);
    fill(states, states + total_indices, 0);

    // Mientras aún hay PCs por leer en el documento
    while (cin >> PC){
        // En cada iteración se entra a un branch, por lo que se le suma 1 a la cantidad total de branches y se calcula el índice con el PC
        branches++;
        pht_index = get_bits(PC, s); // Cálculo de índice del PHT
        bht_index = pht_index ^ patterns[ pht_index ]; // Cálculo del índice del BHT

        // Si la predicción tiene un 0 en el bit más significativo, es un not taken. Caso contrario es un taken
        prediction = (states[bht_index] < 2) ? 'N':'T';

        // Se carga el resultado del documento a la variable documented_outcome
        cin >> documented_outcome;

        // Se desplaza el registro de historia accesado 1 bit a la izquierda, después se le suma 1 si documented_outcome es 1
        patterns[ pht_index ] = get_bits( ( patterns[ pht_index ]*2 ), ph);

        switch (prediction){
            // Si se predice un not taken
            case 'N':{
                /* Si el documento tenía un taken, el not taken predicho es incorrecto, entonces se incrementa el 2bc correspondiente
                del arreglo y se suma 1 a la cuenta de not taken incorrectos. En este caso no hace falta revisar si el 2bc es mayor
                o menor a algún valor dado que el primer bit es 0 y se le suma 1. Como el branch se tomó, se pone un 1 en el bit
                menos significativo de historia*/
                if (documented_outcome == 'T'){
                    states[bht_index]++;
                    patterns[pht_index]++;
                    incorrect_taken++;
                } // Caso contrario, se le resta 1 al 2bc (si este no se encuentra en strongly not taken) y se suma 1 a los not taken correctos
                else if (documented_outcome == 'N'){
                    correct_not++;
                    if (states[bht_index] > 0){
                        states[bht_index]--;
                    }
                }
                break;
            }
            // Similar al caso 'N'. Se contabilizan los taken correctos y se manejan los 2bc correspondientes al caso.
            case 'T':{
                if (documented_outcome == 'N'){
                    states[bht_index]--;
                    incorrect_not++;
                }
                else if (documented_outcome == 'T'){
                    correct_taken++;
                    patterns[ pht_index ]++;
                    if (states[bht_index] < 3 ){
                        states[bht_index]++;
                    }
                }
                break;
            }
        }
    }
    // Se retornan todas las variables deseadas.
    int returningVals [5] = {branches, correct_taken, incorrect_taken, correct_not, incorrect_not};
    return vector<int> (returningVals, returningVals + sizeof(returningVals)/sizeof(int) );
}

/*
Implementación del predictor Gshare. Toma como entradas la cantidad de bits a tomar del PC (s) y la cantidad de bits del registro de 
historia (gh). Retorna un vector de enteros con los saltos totales realizados, "taken" correctos, "taken" incorrectos, "not taken"
correctos y "not taken" incorrectos. Lee los PC del archivo proveído por medio de un while con cin. Se manejan los índices y contenidos 
de las tablas con variables de tipo int, para facilitar el uso de operaciones "bitwise" de C++ y manejo numérico en general, 
pero la estructura en general obedece los patrones que describen el algoritmo en base binaria.
*/
vector<int> gshare(int s, int gh){
    /* total_indices es la cantidad de entradas que tendrá el arreglo, index es una variable a utilizar para iterar sobre los elementos
    del arreglo, branches es la variable que indicará la cantidad total de saltos realizados, correct_* indica los taken o not taken
    correctos según corresponda, mientras que incorrect_* indica los taken o not taken incorrectos según corresponda*/
    int total_indices = pow(2,s), pc_last_bits = 0, branches = 0, index = 0;
    int correct_taken = 0, correct_not = 0;
    int incorrect_taken = 0, incorrect_not = 0;

    // Como es solo 1 registro de historia global, se puede manejar la historia con una variable simple, manteniendo su valor dentro de 
    // la cantidad de bits definida por gh.
    int history = 0;

    /* PC indica el program counter que se está leyendo del documento, prediction es la predicción que realiza el predictor,
    documented_outcome es el resultado que está registrado en el documento, states es el arreglo que almacenará a todos los 2bc
    del predictor. La función fill preinicializa los contenidos del arreglo al estado Strongly Not Taken (SN), con un valor de 0*/
    string PC = "";
    char prediction, documented_outcome;
    int states [ total_indices ] = {0};
    // fill(states, states + total_indices, 0);

    // Mientras aún hay PCs por leer en el documento
    while (cin >> PC){
        // En cada iteración se entra a un branch, por lo que se le suma 1 a la cantidad total de branches y se calcula el índice con el PC
        branches++;
        pc_last_bits = get_bits(PC, s) ; // Cálculo los últimos s bits del PC a tomar
        index = history ^ pc_last_bits; // Cálculo del índice de acceso del BHT
        history = get_bits( ( history << 1 ), gh); // Se desplaza la historia 1 bit a la izquierda

        // Si la predicción tiene un 0 en el bit más significativo, es un not taken. Caso contrario es un taken
        prediction = (states[index] < 2) ? 'N':'T';

        // Se carga el resultado del documento a la variable documented_outcome
        cin >> documented_outcome;

        // Un switch statement que revisa si las predicciones coinciden con los resultados del documento.
        switch (prediction){
            // Si se predice un not taken
            case 'N':{
                /* Si el documento tenía un taken, el not taken predicho es incorrecto, entonces se incrementa el 2bc correspondiente
                del arreglo y se suma 1 a la cuenta de not taken incorrectos. En este caso no hace falta revisar si el 2bc es mayor
                o menor a algún valor dado que el primer bit es 0 y se le suma 1*/
                if (documented_outcome == 'T'){
                    states[index]++;
                    history++; // Si hay un taken, se le pone un 1 al bit menos significativo de historia
                    incorrect_taken++;
                } // Caso contrario, se le resta 1 al 2bc (si este no se encuentra en strongly not taken) y se suma 1 a los not taken correctos
                else if (documented_outcome == 'N'){
                    correct_not++;
                    if (states[index] > 0){
                        states[index]--;
                    }
                }
                break;
            }
            // Similar al caso 'N'. Se contabilizan los taken correctos y se manejan los 2bc correspondientes al caso.
            case 'T':{
                if (documented_outcome == 'N'){
                    states[index]--;
                    incorrect_not++;
                }
                else if (documented_outcome == 'T'){
                    correct_taken++;
                    history++;
                    if (states[index] < 3 ){
                        states[index]++;
                    }
                }
                break;
            }
        }
    }
    // Se retornan todas las variables deseadas.
    int returningVals [5] = {branches, correct_taken, incorrect_taken, correct_not, incorrect_not};
    return vector<int> (returningVals, returningVals + sizeof(returningVals)/sizeof(int) );
}

/*
Implementación del predictor por torneo. Toma como entradas la cantidad de bits a tomar del PC (s), la cantidad de bits del registro de 
historia global (gh) y la cantidad de bits de los registros de historia privada (ph). Retorna un vector de enteros con los saltos 
totales realizados, "taken" correctos, "taken" incorrectos, "not taken" correctos y "not taken" incorrectos. Lee los PC del archivo 
proveído por medio de un while con cin. Se manejan los índices y contenidos de las tablas con variables de tipo int, para facilitar el uso de 
operaciones "bitwise" de C++ y manejo numérico en general, pero la estructura en general obedece los patrones que describen el algoritmo 
en base binaria.
*/
vector<int> tournament(int s, int ph, int gh){
    int total_indices = pow(2,s), pc_last_bits, p_bht_index, g_index;
    int branches = 0, correct_taken = 0, correct_not = 0;
    int incorrect_taken = 0, incorrect_not = 0;
    char preferred_predic;

    string PC = "";
    char p_prediction, g_prediction, prediction, documented_outcome;
    int g_history = 0;
    int p_history [total_indices];
    int p_states [total_indices];
    int g_states[total_indices];
    int meta_pred [total_indices];
    fill(p_history, p_history + total_indices, 0);
    fill(p_states, p_states + total_indices, 0);
    fill(g_states, g_states + total_indices, 0);
    fill(meta_pred, meta_pred + total_indices, 3);

    // Mientras aún hay PCs por leer en el documento
    while (cin >> PC){
        // En cada iteración se entra a un branch, por lo que se le suma 1 a la cantidad total de branches y se calcula el índice con el PC
        branches++;
        cin >> documented_outcome;

        // ******************************ENTRENAMIENTO PSHARE******************************
        pc_last_bits = get_bits(PC, s); // Cálculo de índice del PHT
        p_bht_index = pc_last_bits ^ p_history[ pc_last_bits ]; // Índice del BHT
        p_prediction = (p_states[p_bht_index] < 2) ? 'N':'T'; // Escogencia de la predicción, dependiendo del valor del 2bc
        p_history[ pc_last_bits ] = get_bits( ( p_history[ pc_last_bits ] << 1 ), ph); // Shift para manejar la historia del salto.

        switch (p_prediction){
            // Si se predice un not taken
            case 'N':{
                /* Si el documento tenía un taken, el not taken predicho es incorrecto, entonces se incrementa el 2bc correspondiente
                del arreglo y se suma 1 a la cuenta de not taken incorrectos. En este caso no hace falta revisar si el 2bc es mayor
                o menor a algún valor dado que el primer bit es 0 y se le suma 1*/
                if (documented_outcome == 'T'){
                    p_states[p_bht_index]++;
                    p_history[pc_last_bits]++;
                } // Caso contrario, se le resta 1 al 2bc (si este no se encuentra en strongly not taken) 
                else if (documented_outcome == 'N'){
                    if (p_states[p_bht_index] > 0){
                        p_states[p_bht_index]--;
                    }
                }
                break;
            }
            // Similar al caso 'N'. Se contabilizan los taken correctos y se manejan los 2bc correspondientes al caso.
            case 'T':{
                if (documented_outcome == 'N'){
                    p_states[p_bht_index]--;
                }
                else if (documented_outcome == 'T'){
                    p_history[ pc_last_bits ]++;
                    if (p_states[p_bht_index] < 3 ){
                        p_states[p_bht_index]++;
                    }
                }
                break;
            }
        }

        // ********************************ENTRENAMIENTO GSHARE********************************
        g_index = g_history ^ pc_last_bits; // Cálculo del índice de acceso del BHT
        // Si la predicción tiene un 0 en el bit más significativo, es un not taken. Caso contrario es un taken
        g_prediction = (g_states[g_index] < 2) ? 'N':'T';
        
        g_history = get_bits( ( g_history << 1 ), gh); // Se desplaza la historia 1 bit a la izquierda, para manejo de historia

        switch (g_prediction){
            // Si se predice un not taken
            case 'N':{
                /* Si el documento tenía un taken, el not taken predicho es incorrecto, entonces se incrementa el 2bc correspondiente
                del arreglo y se suma 1 a la cuenta de not taken incorrectos. En este caso no hace falta revisar si el 2bc es mayor
                o menor a algún valor dado que el primer bit es 0 y se le suma 1*/
                if (documented_outcome == 'T'){
                    g_states[g_index]++;
                    g_history++; // Si hay un taken, se le pone un 1 al bit menos significativo de historia
                } // Caso contrario, se le resta 1 al 2bc (si este no se encuentra en strongly not taken) y se suma 1 a los not taken correctos
                else if (documented_outcome == 'N'){
                    if (g_states[g_index] > 0){
                        g_states[g_index]--;
                    }
                }
                break;
            }
            // Similar al caso 'N'. Se contabilizan los taken correctos y se manejan los 2bc correspondientes al caso.
            case 'T':{
                if (documented_outcome == 'N'){
                    g_states[g_index]--;
                }
                else if (documented_outcome == 'T'){
                    g_history++;
                    if (g_states[g_index] < 3 ){
                        g_states[g_index]++;
                    }
                }
                break;
            }
        }
        // ********************************ENTRENAMIENTO METAPREDICTOR********************************
        preferred_predic = (meta_pred[pc_last_bits]<2) ? 'G':'P'; // Se escoge el predictor preferido según la tabla indexada por PC

        switch (preferred_predic){
        // Caso del Gshared
        case 'G':{
            prediction = g_prediction;
            if (prediction == documented_outcome){ // Si el Gshared es correcto, su decisión se toma para contar las decisiones correctas
                switch (prediction){
                case 'T':
                    correct_taken++;
                    break;
                case 'N':
                    correct_not++;
                    break;
                }
                if (meta_pred[pc_last_bits] > 0 && (p_prediction != documented_outcome) ){
                    meta_pred[pc_last_bits]--; // Si no es strongly prefer Gshared y el otro predictor se equivocó, se prefiere Gshared
                }
            } else{ // Caso de que Gshared se equivocó. Su decisión se toma para contar las decisiones incorrectas
                switch (prediction){
                case 'T':
                    incorrect_not++;
                    break;
                case 'N':
                    incorrect_taken++;
                    break;
                }
                if (meta_pred[pc_last_bits] < 3 && (p_prediction == documented_outcome) ){
                    meta_pred[pc_last_bits]++; // Si no es strongly prefer Pshared y el Pshared acertó, se mueve la preferencia a Pshared
                }
            }
            break;
        }
        case 'P': // Similar al caso anterior, con preferencias del Pshared
            prediction = p_prediction; // Si el preferido es Pshared, la predicción es la misma que el Pshared.
            if (prediction == documented_outcome){
                switch (prediction){
                case 'T':
                    correct_taken++;
                    break;
                case 'N':
                    correct_not++;
                    break;
                }
                if (meta_pred[pc_last_bits] < 3 && (g_prediction != documented_outcome) ){
                    meta_pred[pc_last_bits]++;
                }
                
            } else{
                switch (prediction){
                case 'T':
                    incorrect_not++;
                    break;
                case 'N':
                    incorrect_taken++;
                    break;
                }
                if (meta_pred[pc_last_bits] > 0 && (g_prediction == documented_outcome) ){
                    meta_pred[pc_last_bits]--;
                }
                
            }
            break;
        }
    }
    // Se retornan todas las variables deseadas.
    int returningVals [5] = {branches, correct_taken, incorrect_taken, correct_not, incorrect_not};
    return vector<int> (returningVals, returningVals + sizeof(returningVals)/sizeof(int) );
}