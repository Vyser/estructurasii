#include "predictors.h" // Si sale un warning, no importa, en el Makefile se soluciona

int main(int argc, char** argv){
    int s = 0, bp = 0, gh = 0, ph = 0;
    string predictor;
    vector<int> valsVec;

    // Lectura de los flags. Se inicia i en 1 dado que el argumento 0 es el nombre del programa
    for (int i = 1; i < argc; i++){
        if ( strcmp( argv[i], "-s") == 0 ) {s = stoi(argv[i+1]);}
        else if ( strcmp( argv[i], "-bp") == 0 ) {bp = stoi(argv[i+1]);}
        else if ( strcmp( argv[i], "-gh") == 0 ) {gh = stoi(argv[i+1]);}
        else if ( strcmp( argv[i], "-ph") == 0 ) {ph = stoi(argv[i+1]);}
        int b = 10;
    }
    switch (bp){
        case 0:{
            valsVec = bimodal(s);
            predictor = "Bimodal";
            break;
        }
        case 1:{
            valsVec = pshare(s, ph);
            predictor = "PShare";
            break;
        }
        case 2:{
            valsVec = gshare(s, gh);
            predictor = "GShare";
            break;
        }
        case 3:{
            valsVec = tournament(s, ph, gh);
            predictor = "Tournament";
            break;
        }
    }

    print_table(predictor, s, ph, gh, valsVec[0], valsVec[1], valsVec[2], valsVec[3], valsVec[4]);

    return 0;
}