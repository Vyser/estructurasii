# Tarea 1: Predictores de saltos

En este repositorio se halla el código implementado en C++ para simular cuatro predictores de saltos: bimodal, Pshare, Gshare y de torneo. La implementación de cada uno se realizó en una función cada uno y estas funciones se hallan en [src/predictors.cpp](./src/predictors.cpp). Para el manejo de bits, se utilizó la representación por bits de números enteros en C++ con [operadores lógicos *bitwise*](https://www.geeksforgeeks.org/bitwise-operators-in-c-cpp/).

La carpeta [include](./include) contiene los archivos *header* que utiliza el programa principal. En la carpeta [src](./src) se halla todo el código fuente desarrollado para la simulación. En la carpeta [build](./build) se hallan archivos de extensión .o (*object files*) que son generados por el compilador. Estos archivos son unidos por el *linker* y generan el archivo ejecutable final. Por esto último, si se desean realizar pruebas "desde 0", puede considerarse ejecutar "make clean" antes de ejecutar "make", para asegurar que el archivo ejecutable final es realmente el que se genera a partir del código.

Para la ejecución del programa se necesitan las siguientes bibliotecas:

* iostream: Biblioteca de manejo de entrada/salida en C++.
* string: Biblioteca para manejo de strings en C++.
* cstring: Utilizado únicamente para comparación de strings en el [main](./src/main.cpp).
* vector: Biblioteca estándar para implementación de vectores en C++.
* cmath: Biblioteca con utilidades y funciones matemáticas. Usada más que nada por la función pow, para calcular potencias de un número.
* iomanip: Utilizado únicamente para manejo del formato de la tabla impresa por el programa (para redondeo a 2 lugares decimales).

Para el manejo de los bits de 2 contadores, cabe resaltar la "traducción" entre decimal y binario utilizada:

| Base 10 | Base 2 | Preferencia en branches | Preferencia en metapredictor de torneo |
| :-: | :-: | :-: | :-: |
| 0 | 00 | Strongly not taken | Strongly prefer Gshared |
| 1 | 01 | Weakly not taken | Weakly prefer Gshared |
| 2 | 10 | Weakly taken | Weakly prefer Pshared |
| 3 | 11 | Strongly taken | Strongly prefer Pshared |